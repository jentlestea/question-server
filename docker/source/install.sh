#!/bin/sh

CONFPATH=/etc/profile.d/qs.sh

if [ -f $CONFPATH ];then
	exit
fi

if [ $# != 1 ];then
	exit
fi
TARGETDIR=$1

OQROOTFS_HOME=/root/oqrootfs
echo "export WORKON_HOME=$TARGETDIR" >> $CONFPATH
echo "export OQROOTFS_HOME=$OQROOTFS_HOME" >> $CONFPATH

#mkdir -p $OQROOTFS_HOME
echo "export INSTALL_REPO_PATH=$OQROOTFS_HOME/repo" >> $CONFPATH
echo "export INSTALL_TARGET_BRANCH=openEuler-21.03" >> $CONFPATH
echo "export INSTALL_SOURCE_BRANCH=Questions" >> $CONFPATH


echo "export SPYTHONPATH=$TARGETDIR/gRPC:$TARGETDIR/gRPC/proto" >> $CONFPATH
#mkdir -p $OQROOTFS_HOME/pool/ifs
echo "export IFSPATH=$OQROOTFS_HOME/pool/ifs" >> $CONFPATH
if [ -z "$COMMIT_FILE_PATH" ];then
	COMMIT_DIR_PATH=$OQROOTFS_HOME/front
#	mkdir -p $COMMIT_DIR_PATH
	COMMIT_FILE_PATH=$COMMIT_DIR_PATH/import.csv
#	touch $COMMIT_FILE_PATH
fi
echo "export IMPORT_COMMIT_FILE=$COMMIT_FILE_PATH" >> $CONFPATH
if [ -z "$REPORT_DIR_PATH" ];then
	REPORT_DIR_PATH=$OQROOTFS_HOME/front
#	mkdir -p $REPORT_DIR_PATH
fi
echo "export OUTPUT_REPORT_DIR=$REPORT_DIR_PATH" >> $CONFPATH

EXECDIR=/usr/bin
echo "cd $TARGETDIR && python3 ./gRPC/dist_server.py" > $EXECDIR/oqserver
echo "python3 $TARGETDIR/tools/oqserver-tool.py \$@" > $EXECDIR/oqserver-tool
chmod +x $EXECDIR/oqserver
chmod +x $EXECDIR/oqserver-tool
