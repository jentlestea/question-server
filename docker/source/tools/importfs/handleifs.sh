#!/bin/sh

SCRIPTBUGZILLADIR=$INSTALL_REPO_PATH/$INSTALL_SOURCE_BRANCH/script

file=$1
log=$2

VerifyCode=`cat $file | grep 'VerifyCode' | sed -n -e 's/.*VerifyCode: \(.*\)/\1/p'`
cd $INSTALL_REPO_PATH/$INSTALL_SOURCE_BRANCH
found=`git log | grep "VerifyCode: $VerifyCode"`
if [[ -n "$VerifyCode" ]] && [[ -n $found ]];then
	echo 0
	exit
fi
cd - &>/dev/null

if [ -z "$VerifyCode" ];then
	VerifyCode=`echo $(($(date +%s%N)/1000000))`
	sed -i -e "1i VerifyCode: $VerifyCode" $file
fi

ReportedBy=`cat $file | grep 'Reported-by' | sed -n -e 's/.*Reported-by: \(.*\)/\1/p'`
Head=`cat $file | grep 'Head' | sed -n -e 's/.*Head: \(.*\)/\1/p'`
Type=`cat $file | grep 'Type' | sed -n -e 's/.*Type: \(.*\)/\1/p'`
Description=`cat $file | grep 'Description' | sed -n -e 's/.*Description: \(.*\)/\1/p'`
Status=`cat $file | grep 'Status' | sed -n -e 's/.*Status: \(.*\)/\1/p'`
Score=`cat $file | grep 'Score' | sed -n -e 's/.*Score: \(.*\)/\1/p'`

if [ -z "$Head" ] || [ -z "$Type" ] || [ -z "$Description" ];then
	echo '[ERROR] invalid ifs:'$file >> $log
	echo 0
	exit
fi

HeadLine='['$Type']'
if [ -n "$Status" ];then
	HeadLine="$HeadLine ##$Status##"
fi
HeadLineShow="$HeadLine $Head"

delimO='===================================='
delimI='Body'

Body=`sed -n -e '/Body:/,//p' $file | tail -n +2`
BodyShow="$Body"

ReportedByShow="Reported-by: $ReportedBy"
DescriptionShow="Description: $Description"
VerifyCodeShow="VerifyCode: $VerifyCode"
ScoreShow="Score: $Score"

cd $INSTALL_REPO_PATH/$INSTALL_SOURCE_BRANCH
git commit -m "$HeadLineShow" -m "$VerifyCodeShow" -m "$ReportedByShow" -m "$ScoreShow" -m "$DescriptionShow" -m "$delimO" -m "$BodyShow" -m "$delimO" --allow-empty &> /dev/null
commitID=`git log --oneline -1 | awk ' ''{print $1}'`
cd - &>/dev/null

BugzillaID=
#prepare to create bugzilla
if [ -n "$commitID" ];then
	cd $SCRIPTBUGZILLADIR
	echo $commitID > .tmp
	msg=`perl ipatches2ol.pl -f .tmp`
	BugzillaID=`echo $msg | grep 'id:' | sed -n -e 's/.*id: \(.*\)/\1/p'`
	rm .tmp
	cd - &>/dev/null
fi
if [[ -z "$BugzillaID" ]];then
	echo "0 0 0"
	exit
fi
Bugzilla='https://bugzilla.openeuler.org/show_bug.cgi?id='$BugzillaID
BugzillaShow="Bugzilla: $Bugzilla"

#push bugzilla even we change commitID!
cd $INSTALL_REPO_PATH/$INSTALL_SOURCE_BRANCH
git commit --amend -m "$HeadLineShow" -m "$VerifyCodeShow" -m "$ReportedByShow" -m "$ScoreShow" -m "$BugzillaShow" -m "$DescriptionShow" -m "$delimO" -m "$BodyShow" -m "$delimO" --allow-empty &> /dev/null
commitID=`git log --oneline -1 | awk ' ''{print $1}'`
cd - &>/dev/null

if [[ -z "$commitID" ]];then
	echo "0 0 0"
	exit
fi
echo $commitID' '$Bugzilla' '$Score
