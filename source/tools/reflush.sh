#!/bin/sh

time=$(date "+%Y-%m-%d")
log=$WORKON_HOME/log/$time.log

oqserver-tool push-repo 2>&1 >> $log
oqserver-tool import-csv import.csv.tmp 2>&1 >> $log
success=`oqserver-tool reflush-database`
echo $success >> $log
if [ -n `echo $success | grep 'SUCCESS'` ];then
	echo 0
else
	echo -1
fi
