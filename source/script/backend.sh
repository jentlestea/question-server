#!/bin/sh

BKPATH=$OQROOTFS_HOME/backend
time=$(date "+%Y-%m-%d")

mkdir -p $BKPATH/$time

BK1=$WORKON_HOME/dataBase
BK2=$WORKON_HOME/log

if [[ ! -d $BK1 ]] || [[ ! -d $BK2 ]];then
	echo -1
fi

tar -zcvf $BKPATH/$time/bk1.tar.gz $BK1/* 2>&1 > /dev/null
tar -zcvf $BKPATH/$time/bk2.tar.gz $BK2/* 2>&1 > /dev/null

echo 0
