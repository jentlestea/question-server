#!/bin/bash

SCRIPTBUFZILLADIR=$WORKON_HOME/script/createBugzilla

git config --global user.email "kernel@openeuler.org"
git config --global user.name "openEuler kernel(Bobo)"

if [[ ! -d $OQROOTFS_HOME ]] || [[ ! -f $OQROOTFS_HOME/pack.tar ]];then
	echo "$OQROOTFS_HOME not exist"
	exit
fi

# unpack oqrootfs
tar -xvf $OQROOTFS_HOME/pack.tar -C $OQROOTFS_HOME

# init repo
mkdir -p $INSTALL_REPO_PATH/$INSTALL_TARGET_BRANCH
mkdir -p $INSTALL_REPO_PATH/$INSTALL_SOURCE_BRANCH
cd $INSTALL_REPO_PATH/$INSTALL_SOURCE_BRANCH
git init 2>&1 > /dev/null
git remote add question git@gitee.com:jentlestea/question-open-euler-kernel.git 2>&1 > /dev/null
git pull question $INSTALL_SOURCE_BRANCH:$INSTALL_SOURCE_BRANCH 2>&1 > /dev/null
cd - 2>&1 > /dev/null

cd $INSTALL_REPO_PATH/$INSTALL_TARGET_BRANCH
git init 2>&1 > /dev/null
git remote add openeuler git@gitee.com:openeuler/kernel.git 2>&1 > /dev/null
git pull openeuler $INSTALL_TARGET_BRANCH:$INSTALL_TARGET_BRANCH 2>&1 > /dev/null
cd - 2>&1 > /dev/null

cd $INSTALL_REPO_PATH/$INSTALL_SOURCE_BRANCH 2>&1 > /dev/null
mkdir -p script
cp $SCRIPTBUFZILLADIR/* ./script
chmod +x ./script -R
git add -A
git commit -m "[original] push scripts for creating bugzilla" 2>&1 > /dev/null
cd - 2>&1 >/dev/null
