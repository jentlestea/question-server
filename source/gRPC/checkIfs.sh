#!/bin/sh

file=$1

if [ -z "$file" ];then
	echo 0
	exit
fi

Head=`cat $file | grep 'Head' | sed -n -e 's/.*Head: \(.*\)/\1/p'`
Type=`cat $file | grep 'Type' | sed -n -e 's/.*Type: \(.*\)/\1/p'`
#Body=`cat $file | grep 'Body' | sed -n -e 's/.*Body:\(.*\)/\1/p'`
Score=`cat $file | grep 'Score' | sed -n -e 's/.*Score: \(.*\)/\1/p'`
Description=`cat $file | grep 'Description' | sed -n -e 's/.*Description: \(.*\)/\1/p'`
if [[ -z "$Head" ]] || [[ -z "$Type" ]] || [[ -z "$Description" ]] || [[ -z "$Score" ]];then
	rm -f $file
	echo '-1'
	exit
fi

allow='1'
ScoreAllow=('1', '2', '3', '4', '5', '6', '7', '8', '9', '10')
echo "${ScoreAllow[@]}" | grep -wq "$Score" && : || allow='0'

TypeAllow=('LTS','LTS(C)','COURSE','BUG', 'FEATURE')
echo "${TypeAllow[@]}" | grep -wq "$Type" && : || allow='0'

if [ $allow == '0' ];then
	rm $file
	echo -1
	exit
fi

VerifyCode=`echo $(($(date +%s%N)/1000000))`
sed -i -e "1i VerifyCode: $VerifyCode" $file

echo $VerifyCode' '$Score
